import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcronymPipe } from './acronym.pipe';
import { SearchByNamePipe } from './search-by-name.pipe';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [AcronymPipe, SearchByNamePipe, SearchPipe],
  imports: [CommonModule],

  exports: [AcronymPipe, SearchByNamePipe, SearchPipe],
})
export class SharedModule {}
