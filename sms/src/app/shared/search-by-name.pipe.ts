import { Pipe, PipeTransform } from '@angular/core';
import { Student } from '../feature/student/model/Student';
@Pipe({
  name: 'searchByName',
})
export class SearchByNamePipe implements PipeTransform {
  transform(students: Student[] | null, searchName: string): Student[] | null {
    if (!searchName) {
      return students; // If search name is empty, return all students
    }

    return students!.filter((student) =>
      student.name.toLowerCase().includes(searchName.toLowerCase())
    );
  }
}
