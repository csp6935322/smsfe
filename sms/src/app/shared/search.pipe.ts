import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  transform<T>(
    data: any[] | null,
    searchedTerm: string,
    searchBy: string
  ): T[] | null {
    if (!data || !searchedTerm || !searchBy) {
      return data;
    } else {
      searchedTerm = searchedTerm.toLowerCase();
      return data!.filter((item) => {
        if (item[searchBy]) {
          console.log('item[searchBy]', item[searchBy]);
          const resource: string = item[searchBy].toString();
          return resource.toLowerCase().includes(searchedTerm);
        }
        return null;
      });
    }
  }
}
