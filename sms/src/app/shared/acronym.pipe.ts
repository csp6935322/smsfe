import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acronym',
})
export class AcronymPipe implements PipeTransform {
  transform(value: string): string {
    let words: string[];
    words = value.trim().split(' ');

    if (words.length == 1) 
    {
      return words[0];
    } 
    else 
    {
      return words.map((letters) => letters[0]).join('');
    }
  }
}
