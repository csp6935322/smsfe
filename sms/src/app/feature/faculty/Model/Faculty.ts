export interface Faculty {
  id: number;
  name: string;
  city: {
    id: number;
    name: string;
  };
  department: {
    id: number;
    title: string;
  };
  doj: Date;
  dob: Date;
  hod: string;
}
 