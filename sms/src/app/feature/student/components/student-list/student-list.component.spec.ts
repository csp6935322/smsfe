import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StudentListComponent } from './student-list.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppStateService } from 'src/app/core/services/app-state.service';
import { StudentService } from '../../services/student.service';
import { SearchPipe } from 'src/app/shared/search.pipe';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CdkVirtualForOf, ScrollingModule } from '@angular/cdk/scrolling';
describe('StudentListComponent', () => {
  let component: StudentListComponent;
  let fixture: ComponentFixture<StudentListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule,
        CdkVirtualForOf,
        ScrollingModule,
      ],
      declarations: [StudentListComponent, SearchPipe],
      providers: [AppStateService, StudentService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(StudentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
