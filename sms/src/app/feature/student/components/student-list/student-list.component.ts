import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../services/student.service';
import { Student } from '../../model/Student';
import { Observable, map } from 'rxjs';
import { Location } from 'src/app/core/models/Location';
import { AppStateService } from 'src/app/core/services/app-state.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css'],
})
export class StudentListComponent implements OnInit {
  allStudents$ = new Observable<Student[]>();
  searchName: string = '';
  searchBy: string = 'name';
  selectedStudent!: Student;
  propertyList!: string[];
  constructor(
    private appStateService: AppStateService,
    private studentService: StudentService
  ) {}

  //Fetch Selected Locatino and then fetch all respective students
  ngOnInit(): void {
    this.appStateService
      .getSelectedLocation()
      .pipe(
        map((location: Location) =>
          this.studentService.fetchStudentsByLocation(location)
        )
      )
      .subscribe((s) => (this.allStudents$ = s));

    //Setting an initial value of Student which is the 1st object from Student[]
    setTimeout(() => {
      this.allStudents$.subscribe(
        (s) => (
          (this.selectedStudent = s[0] as Student),
          (this.propertyList = Object.keys(this.selectedStudent))
        )
      );
    }, 100);
  }

  //Changing value of selected student on click
  handleClick(student: any): void {
    this.selectedStudent = student;
    this.propertyList = Object.keys(this.selectedStudent);
  }
}
