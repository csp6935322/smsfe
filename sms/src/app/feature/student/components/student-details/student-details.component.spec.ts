import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LeaveService } from '../../services/leave.service';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { StudentDetailsComponent } from './student-details.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatDialogModule,MatDialogRef } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';

describe('StudentDetailsComponent', () => {
  let component: StudentDetailsComponent;
  let fixture: ComponentFixture<StudentDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, MatDialogModule, MatMenuModule],
      declarations: [StudentDetailsComponent],
      providers: [ErrorHandlerService, LeaveService,{provide:MatDialogRef,useValue:{}}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(StudentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
