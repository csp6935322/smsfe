import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Leave } from '../../model/Leave';
import { LeaveService } from '../../services/leave.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { StudentFormComponent } from '../student-form/student-form.component';
@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css'],
})
export class StudentDetailsComponent implements OnInit, OnChanges {
  @Input() selectedStudent: any = {};
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  // Declaring data source for the MatTable to use
  dataSource!: MatTableDataSource<Leave>;
  // All Column names
  displayedColumns: string[] = [
    'ID',
    'Start Date',
    'End Date',
    'Reason',
    'Leave Type',
  ];
  length: number = 0;

  constructor(private leaveService: LeaveService, private dialog: MatDialog) {}

  // At View Initialization fetch all leaves of current student
  ngOnInit(): void {
    // Fetch leaves of currently selected student
    this.leaveService
      .fetchLeaves(this.selectedStudent?.id)
      .subscribe((leaves) => {
        this.dataSource = new MatTableDataSource(leaves);
      });
    // Set lenght of the incoming data to local variable length
    this.length = this.dataSource?.data.length;
    // Using setTimeout we're delaying setting paginator to handle late response from server
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 400);
  }

  // Update data on selection change
  ngOnChanges(changes: SimpleChanges): void {
    //Fetch leaves of currently selected student changed
    if (changes['selectedStudent'].currentValue?.id) {
      this.leaveService
        .fetchLeaves(changes['selectedStudent'].currentValue.id)
        .subscribe((leaves) => {
          this.dataSource.data = leaves;
          this.dataSource.paginator = this.paginator;
        });
    }

    setTimeout(() => {
      this.length = this.dataSource?.data.length;
    }, 50);
  }

  // Implement default sort
  sortData() {
    // Set the MatSort instance of the data source
    this.dataSource.sort = this.sort;
    // Apply sorting to the filtered data
    this.dataSource.sortData(
      this.dataSource.filteredData,
      this.dataSource.sort
    );
  }

  // Openeing StudentForm Dialog
  openStudentDialog(event: any) {
    if (event) {
      this.dialog.open(StudentFormComponent, {
        width: 'auto',
        data: {
          selectedStudent: this.selectedStudent,
          operationType: event === 'add' ? 'add' : 'update',
        },
        disableClose: true,
      });
    }
  }
}
