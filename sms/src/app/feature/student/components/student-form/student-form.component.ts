import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Department } from 'src/app/feature/department/Model/Department';
import { DeptService } from 'src/app/feature/department/services/dept.service';
import { Location } from 'src/app/core/models/Location';
import { LocationListService } from 'src/app/core/services/location-list.service';
import { Student } from '../../model/Student';
import { StudentService } from '../../services/student.service';
import {
  FormBuilder,
  FormControl,
  ValidatorFn,
  AbstractControl,
  Validators,
  FormGroup,
} from '@angular/forms';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css'],
})
export class StudentFormComponent implements OnInit {
  studentForm!: FormGroup;
  location!: Location;
  locations!: Location[];
  department!: Department;
  departments!: Department[];
  maxJoiningDate!: Date;
  maxDOBDate!: Date;
  mode: string = '';
  shouldFormSubmit: boolean = true;
  student!: Student;
  
  constructor(
    private formBuilder: FormBuilder,
    private locationService: LocationListService,
    private deptService: DeptService,
    private studentService: StudentService,
    private dialogRef: MatDialogRef<StudentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.mode = this.data?.operationType;
    this.student = this.data?.selectedStudent;
    //Set value of local variable location:Location with incoming data
    this.location = this.student?.location;
    //Set value of local variable department:Department with incoming data
    this.department = this.student?.department;

    //Getting all departments
    this.deptService
      .fetchAllDept()
      .subscribe((depts) => (this.departments = depts));

    //Getting all locations
    this.locationService
      .fetchAllLocations()
      .subscribe((allLocations) => (this.locations = allLocations));

    //Fetch Operation Type
    this.mode = this.data.operationType;
    //Set maximum date of joining less than today
    this.maxJoiningDate = new Date();
    //Set maximum date of birth as 16 yrs in past from today
    this.maxDOBDate = new Date();
    this.maxDOBDate.setFullYear(this.maxDOBDate.getFullYear() - 16);

    if (this.mode == 'add') {
      this.studentForm = this.formBuilder.group({
        name: new FormControl('', Validators.required),
        location: new FormControl('', Validators.required),
        dob: new FormControl('', Validators.required),
        doj: new FormControl('', [Validators.required, this.dojValidator()]),
        department: new FormControl('', Validators.required),
        grades: new FormControl('', [
          Validators.required,
          Validators.pattern(/^\d(\.\d)?$/),
        ]),
        semester: new FormControl('', Validators.required),
        hod: new FormControl('', Validators.required),
      });
    } else {
      //Initializing FormControls with initial values
      this.studentForm = this.formBuilder.group({
        id: new FormControl(`${this.student?.id}`, Validators.required),
        name: new FormControl(`${this.student?.name}`, Validators.required),
        location: new FormControl(`${this.location?.name}`),
        doj: new FormControl(`${this.student?.doj}`, Validators.required),
        dob: new FormControl(`${this.student?.dob}`, [
          Validators.required,
          this.dojValidator(),
        ]),
        department: new FormControl(
          `${this.department?.title}`,
          Validators.required
        ),
        grades: new FormControl(`${this.student?.grades}`, [
          Validators.required,
          Validators.pattern(/^\d(\.\d)?$/),
        ]),
        semester: new FormControl('', Validators.required),
        hod: new FormControl(`${this.department?.hod}`, Validators.required),
      });

      //Setting Initial value of semester here,
      //because until template finishesh iterating over loop, this value will not be overwritten
      this.studentForm?.get('semester')?.setValue(this.student?.semester);
    }
  }

  //Select respective HOD when user selects a Department
  handleDeptInput() {
    const selectedDepartment = this.studentForm?.get('department')?.value;
    this.department =
      this.departments.find((dept) => dept.title === selectedDepartment) ||
      this.department;
    this.studentForm?.get('hod')?.setValue(this.department!.hod);
  }

  //Select location as per user
  handleLocationInput() {
    const selectedLocation = this.studentForm?.get('location')?.value;
    this.location =
      this.locations.find((location) => location.name === selectedLocation) ||
      this.location;
  }

  // Based operation perfom POST or PUT
  handleSubmit() {
    if (this.shouldFormSubmit) {
      const formData = this.studentForm?.value;
      this.student = {
        id: Number.parseInt(formData.id || ''),
        name: formData.name || '',
        dob: new Date(formData.dob || ''), // Convert the string to a Date object
        doj: new Date(formData.doj || ''), // Convert the string to a Date object
        grades: Number.parseFloat(formData.grades || ''),
        semester: Number.parseInt(formData.semester || ''),
        location: this.location,
        department: this.department,
      };

      if (this.mode == 'add') {
        this.studentService.addStudent(this.student);
      } else {
        this.studentService.updateStudent(this.student);
      }
      this.handleGoBack();
      window.location.reload();
    }
  }

  // Go back to the Parent component from where Dialog was opened
  handleGoBack() {
    this.dialogRef.close();
  }

  //Custom validatory function to check that the DOJ is at least 16 yrs later than DOB
  dojValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const dob = new Date(this.studentForm?.get('dob')?.value || '');
      const doj = new Date(this.studentForm?.get('doj')?.value || '');
      const validAfterThisDate: Date = new Date(
        `${dob.getFullYear() + 16}/${dob.getMonth() + 1}/${dob.getDate()}`
      );

      if (doj > validAfterThisDate) {
        return null;
      } else {
        return { dojInvalid: true };
      }
    };
  }
}
