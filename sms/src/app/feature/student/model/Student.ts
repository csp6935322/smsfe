export interface Student {
  id: number;
  name: string;
  location: {
    id: number;
    name: string;
  };
  doj: Date;
  dob: Date;
  department: {
    id: number;
    title: string;
    hod: string;
  };
  grades: number;
  semester: number;
}
