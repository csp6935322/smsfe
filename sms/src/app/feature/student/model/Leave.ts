export interface Leave {
  id: number;
  studentId: number;
  startDate: Date;
  endDate: Date;
  reason: string;
  type: string;
}
