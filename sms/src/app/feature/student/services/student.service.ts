import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Student } from '../model/Student';
import { Location } from 'src/app/core/models/Location';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };
  studentUrl = 'http://localhost:3000/students';
  selectedStudent: Student = <Student>{};

  constructor(
    private errorHandler: ErrorHandlerService,
    private http: HttpClient
  ) {}

  //Fetch student by specific Id
  fetchStudentById(id: number): Observable<Student> {
    const url = `${this.studentUrl}/${id}`;
    return this.http.get<Student>(url).pipe(
      tap((_) => 'Fetched Student: id = ' + id),
      catchError(
        this.errorHandler.handleApiError<Student>(
          'fetchStudentById(id: number)'
        )
      )
    );
  }

  //Fetch all students
  fetchAllStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(this.studentUrl).pipe(
      tap((_) => 'Fetched All Students'),
      catchError(
        this.errorHandler.handleApiError<Student[]>('fetchAllStudents', [])
      )
    );
  }

  //Fetch all students belonging to specific location
  fetchStudentsByLocation(location: Location): Observable<Student[]> {
    const apiUrl = `${this.studentUrl}?location.id=${location.id}`;
    return this.http.get<Student[]>(apiUrl).pipe(
      tap((_) => 'Fetched students with: location = ' + location.name),
      catchError(
        this.errorHandler.handleApiError(
          'fetchStudentsByLocation(location: Location)',
          []
        )
      )
    );
  }

  //POST request for Model: Student
  addStudent(student: Student): Subscription {
    return this.http
      .post<Student>(this.studentUrl, student, this.httpOptions)
      .pipe(
        tap((newStudent: Student) => `added Student w/ id=${newStudent.id}`),
        catchError(
          this.errorHandler.handleApiError<Student>(
            'addStudent(student: Student):'
          )
        )
      )
      .subscribe();
  }

  //POST request for Model: Student
  updateStudent(student: Student): Subscription {
    //Form valid url for PUT mapping
    const putUrl = `${this.studentUrl}/${student.id}`;
    return this.http
      .put<Student>(putUrl, student, this.httpOptions)
      .pipe(
        tap((_) => `updated Student with id=${student.id}`),
        catchError(
          this.errorHandler.handleApiError<Student>(
            ' updateStudent(student: Student)'
          )
        )
      )
      .subscribe();
  }
}
