import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Leave } from '../model/Leave';
import { Observable, tap, catchError } from 'rxjs';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
@Injectable({
  providedIn: 'root',
})
export class LeaveService {
  constructor(
    private errorHandler: ErrorHandlerService,
    private http: HttpClient
  ) {}

  //Fetch all leaves for Student with specific Id
  fetchLeaves(id: number): Observable<Leave[]> {
    const api = `http://localhost:3000/leaves?studentId=${id}`;
    return this.http.get<Leave[]>(api).pipe(
      tap((_) => 'Fetched Leaves with: Student Id = ' + id),
      catchError(
        this.errorHandler.handleApiError<Leave[]>('getLeaves(id: number)', [])
      )
    );
  }
}
