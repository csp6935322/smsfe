import { TestBed } from '@angular/core/testing';
import { LeaveService } from './leave.service';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';

describe('LeaveService', () => {
  let service: LeaveService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ErrorHandlerService],
    });
    service = TestBed.inject(LeaveService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
