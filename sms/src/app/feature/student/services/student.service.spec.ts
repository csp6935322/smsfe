import { TestBed } from '@angular/core/testing';
import { Student } from '../model/Student';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { StudentService } from './student.service';
import { catchError } from 'rxjs';

describe('StudentService', () => {
  let service: StudentService;
  let erroHandler: ErrorHandlerService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ErrorHandlerService],
    });
    service = TestBed.inject(StudentService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('fetchAllStudents', () => {
    let mockStudents: Student[];

    beforeEach(() => {
      mockStudents = [
        {
          id: 1,
          name: 'Aarav Kumar',
          dob: new Date('1998-05-12T00:00:00.000Z'),
          doj: new Date('2022-04-12T00:00:00.000Z'),
          grades: 9,
          semester: 8,
          location: {
            id: 1,
            name: 'Kolhapur',
          },
          department: {
            id: 4,
            title: 'Civil',
            hod: 'Dr. Prakash More',
          },
        },
        {
          id: 2,
          name: 'Saanvi Patil',
          location: {
            id: 2,
            name: 'Pune',
          },
          dob: new Date('1999-07-21'),
          doj: new Date('2001-02-10'),
          department: {
            id: 2,
            title: 'Agriculture',
            hod: 'Dr. Prakash Joshi',
          },
          grades: 7.2,
          semester: 4,
        },
      ];
    });

    it('should return expected Students by calling once', () => {
      service.fetchAllStudents().subscribe((response) => {
        expect(response).toEqual(mockStudents), fail;
      });

      const testRequest = httpTestingController.expectOne(service.studentUrl);
      expect(testRequest.request.method).toBe('GET');

      testRequest.flush(mockStudents);
    });

    it('should not fail while returning Empty Array of Students', () => {
      service.fetchAllStudents().subscribe((response) => {
        expect(response.length).toEqual(0);
        fail;
      });

      const testRequest = httpTestingController.expectOne(service.studentUrl);

      expect(testRequest.request.method).toBe('GET');
      testRequest.flush([]);
    });

    it('should return empty array for Status Code: 404', () => {
      service.fetchAllStudents().subscribe((response) => {
        expect(response.length).toBe(0);
        fail;
      });

      const testRequest = httpTestingController.expectOne(service.studentUrl);
      testRequest.flush('404 error', { status: 404, statusText: 'Not Found' });
    });

    it('should return same expected results when called multiple times', () => {
      service.fetchAllStudents().subscribe();
      service.fetchAllStudents().subscribe((response) => {
        expect(response).toEqual(mockStudents);
        fail;
      });

      const testRequests = httpTestingController.match(service.studentUrl);
      expect(testRequests.length).toBe(2);

      testRequests[0].flush([]);
      testRequests[1].flush(mockStudents);
    });
  });

  describe('fetchstudentById', () => {
    let mockStudent: Student;
    const id = 1;
    beforeEach(() => {
      mockStudent = {
        id: 1,
        name: 'Aarav Kumar',
        dob: new Date('1998-05-12T00:00:00.000Z'),
        doj: new Date('2022-04-12T00:00:00.000Z'),
        grades: 9,
        semester: 8,
        location: {
          id: 1,
          name: 'Kolhapur',
        },
        department: {
          id: 4,
          title: 'Civil',
          hod: 'Dr. Prakash More',
        },
      };
    });

    it('should return expected Student when called once', () => {
      service.fetchStudentById(id).subscribe((response) => {
        expect(response).toEqual(mockStudent);
      });

      const testReq = httpTestingController.expectOne(
        `${service.studentUrl}/${id}`
      );
      expect(testReq.request.method).toBe('GET');

      testReq.flush(mockStudent);
    });

    it('should able to return empty object', () => {
      service.fetchStudentById(0).subscribe((response) => {
        expect(response).toEqual({} as Student);
      });

      const testReq = httpTestingController.expectOne(
        `${service.studentUrl}/${0}`
      );

      testReq.flush({});
    });

    it('should return empty object when hit 404 Error', () => {
      service.fetchStudentById(0).subscribe((response) => {
        expect(response).toEqual({} as Student);
        fail;
      });

      const testReq = httpTestingController.expectOne(
        `${service.studentUrl}/${0}`
      );
      testReq.flush({}, { status: 404, statusText: 'Not Found' });
    });
  });
});
