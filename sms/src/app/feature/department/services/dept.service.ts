import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Department } from '../Model/Department';
import { Observable,tap } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class DeptService {
  constructor(private http: HttpClient) {}

  deptUrl = 'http://localhost:3000/departments';

  fetchAllDept(): Observable<Department[]> {
    return this.http.get<Department[]>(this.deptUrl).pipe(
      tap(_=>{ 'Fetched all Departments'})
    );
  }
}
 