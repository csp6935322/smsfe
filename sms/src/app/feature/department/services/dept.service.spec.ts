import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { DeptService } from './dept.service';
import { MatDialogModule } from '@angular/material/dialog';
describe('DeptService', () => {
  let service: DeptService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [ErrorHandlerService],
    });
    service = TestBed.inject(DeptService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make a GET request', () => {
    const mockResponse = [
      {
        id: 1,
        title: 'Computer Science',
        hod: 'Dr. Rajesh Deshmukh',
      },
      {
        id: 2,
        title: 'Agriculture',
        hod: 'Dr. Prakash Joshi',
      },
    ];

    service.fetchAllDept().subscribe((res) => {
      expect(res).toEqual(mockResponse);
    });

    const req = httpTestingController.expectOne(
      'http://localhost:3000/departments'
    );

    expect(req.request.method).toBe('GET');

    req.flush(mockResponse);
  });
});
