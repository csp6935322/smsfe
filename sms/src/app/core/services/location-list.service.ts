import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Location } from '../models/Location';
import { Observable, map, tap, catchError } from 'rxjs';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root',
})
export class LocationListService {
  private locationUrl = 'http://localhost:3000/locations';
  locations: Location[] = [];

  constructor(
    private errorHandler: ErrorHandlerService,
    private http: HttpClient
  ) {}

  //Fetch all locations at the app start-up
  fetchAllLocations(): Observable<Location[]> {
    return this.http.get<Location[]>(this.locationUrl).pipe(
      tap((_) => 'fetched locations'),
      map((locations: any[]) => {
        // Converting the locations into an array of Location objects
        return locations.map((location) => ({
          id: location.id,
          name: location.name,
        }));
      }),
      catchError(
        this.errorHandler.handleApiError<Location[]>(' fetchAllLocations', [])
      )
    );
  }
}
