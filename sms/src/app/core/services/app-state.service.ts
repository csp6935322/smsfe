import { Injectable } from '@angular/core';
import { Location } from 'src/app/core/models/Location';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppStateService {
  selectedLocation$ = new BehaviorSubject<Location>({
    id: 1,
    name: 'Kolhapur',
  });

  setSelectedLocation(location: Location) {
    this.selectedLocation$.next(location);
  }

  getSelectedLocation(): BehaviorSubject<Location> {
    return this.selectedLocation$;
  }
}
