import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ErrorHandlerService } from 'src/app/core/services/error-handler.service';
import { LocationListService } from './location-list.service';

describe('LocationListService', () => {
  let service: LocationListService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ErrorHandlerService, LocationListService],
    });
    service = TestBed.inject(LocationListService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make a GET request', () => {
    const mockRes: any = [];
    service.fetchAllLocations().subscribe((response) => {
      expect(response).toEqual(mockRes);
    });

    const request = httpTestingController.expectOne(
      'http://localhost:3000/locations'
    );
    expect(request.request.method).toBe('GET');
    request.flush(mockRes);
  });
});
 