import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService {
  //Error handler service
  handleApiError<T>(opearation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${opearation} failed::\n`, error);
      return of(result as T);
    };
  }
}
