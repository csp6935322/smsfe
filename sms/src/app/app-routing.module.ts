import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'student',
    loadChildren: () =>
      import('./feature/student/student.module').then((m) => m.StudentModule),
  },
  {
    path: 'faculty',
    loadChildren: () =>
      import('./feature/faculty/faculty.module').then((m) => m.FacultyModule),
  },
  {
    path: 'department',
    loadChildren: () =>
      import('./feature/department/department.module').then(
        (m) => m.DepartmentModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
