import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StudentModule } from './feature/student/student.module';
import { AppViewComponent } from './main-view/main-view.component';
import { SideNavComponent } from './main-view/components/side-nav/side-nav.component';
import { LocationListComponent } from './main-view/components/location-list/location-list.component';
import { ErrorHandlerService } from './core/services/error-handler.service';

@NgModule({
  declarations: [
    AppComponent,
    AppViewComponent,
    LocationListComponent,
    SideNavComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    StudentModule,
  ],
  providers: [ErrorHandlerService,HttpClientModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
