import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppViewComponent } from './main-view.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AppViewComponent', () => {
  let component: AppViewComponent;
  let fixture: ComponentFixture<AppViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AppViewComponent],
      schemas:[CUSTOM_ELEMENTS_SCHEMA]
    });
    fixture = TestBed.createComponent(AppViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
