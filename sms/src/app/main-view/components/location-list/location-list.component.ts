import { Component, OnInit } from '@angular/core';
import { Location } from 'src/app/core/models/Location';
import { LocationListService } from 'src/app/core/services/location-list.service';
import { AppStateService } from 'src/app/core/services/app-state.service';

@Component({
  selector: 'app-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.css'],
})
export class LocationListComponent implements OnInit {
  locations: Location[] = [];
  selectedLocation: Location = { id: 1, name: 'Kolhapur' };

  constructor(
    private locationListService: LocationListService,
    private appStateService: AppStateService
  ) {}

  //Fetch all Locations on View Start up
  ngOnInit(): void {
    this.locationListService
      .fetchAllLocations()
      .pipe()
      .subscribe((allLocations: Location[]) => {
        this.locations = allLocations;
      });
  }

  //Change app-state location
  handleLocationSelect(): void {
    this.appStateService.setSelectedLocation(this.selectedLocation);
  }
}
