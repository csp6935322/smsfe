module.exports = function (config) {
    config.set({
      frameworks: ['jasmine'],
      files: [
        'src/**/*.spec.ts' 
      ],
      browsers: ['Chrome'], 
      reporters: ['spec', 'coverage'],
      preprocessors: {
        'src/**/*.spec.ts': ['coverage']
      },
      coverageReporter: {
        type: 'html',
        dir: 'coverage/'
      },
      singleRun: true
    });
  };
  